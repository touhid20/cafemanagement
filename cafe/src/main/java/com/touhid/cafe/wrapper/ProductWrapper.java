package com.touhid.cafe.wrapper;

import lombok.Data;

@Data
public class ProductWrapper {
	Integer id;
	
	String name;
	
	String description;
	
	Integer price;
	
	String status;
	
	Integer categoryId;
	
	String categoryName;
	
	public ProductWrapper() {
		
	}
//	(p.id,p.name,p.description,p.price,p.status,p.category.id,p.category.name) from Product p")
	public ProductWrapper(Integer id, String name, String description, Integer price, String status, Integer categoryId,
			String categoryName) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.status = status;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
	}
public ProductWrapper(Integer id, String name) {
	this.id = id;
	this.name = name;
}
public ProductWrapper(Integer id, String name, String description, Integer price) {
	super();
	this.id = id;
	this.name = name;
	this.description = description;
	this.price = price;
}
	

	
	

}
