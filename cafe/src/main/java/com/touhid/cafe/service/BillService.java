package com.touhid.cafe.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.touhid.cafe.POJO.Bill;

public interface BillService {

	ResponseEntity<String> generateReport(Map<String, Object> requestMap);
	
	ResponseEntity<List<Bill>> getBills();
	
	ResponseEntity<Byte[]> getPdf(Map<String, Object> requestMap);
	
	ResponseEntity<String> deleteBill(Integer id);

}
 